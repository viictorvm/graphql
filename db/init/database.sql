CREATE DATABASE database1;
\connect database1;

CREATE TABLE table1 (
    id SERIAL PRIMARY KEY,
    name TEXT,
    description TEXT,
    created_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE table2 (
    id SERIAL PRIMARY KEY,
    name TEXT,
    description TEXT,
    created_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    table1_id INTEGER NOT NULL REFERENCES table1(id)
);

INSERT INTO table1 (name, description) VALUES
('Table1 name 1', 'Table1 description 1'),
('Table1 name 2', 'Table1 description 2'),
('Table1 name 3', 'Table1 description 3');

INSERT INTO table2 (name, description, table1_id) VALUES
('Table2 name 1', 'Table2 description 1', 1),
('Table2 name 2', 'Table2 description 2', 2),
('Table2 name 3', 'Table2 description 3', 3);