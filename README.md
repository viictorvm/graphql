# Onboarding GraphQL

1. Copy `.env.dist` to `.env` and on the root folder execute: `make start`.
2. Go to http://0.0.0.0:PORT_GRAPHQL/graphiql, in this example is 5000.
3. Execute some queries. For example:
```
query {
  allTable1S{
    nodes {
      name
      description
      
    }
  }
}
```
