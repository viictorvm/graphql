
.PHONY: start
start: stop erase build up

.PHONY: start
start: stop erase build up

.PHONY: up
up:
		docker-compose up -d

.PHONY: stop
stop:
		docker-compose stop

.PHONY: erase
erase:
		docker-compose down -v

.PHONY: build
build:
		docker-compose build && \
		docker-compose pull

.PHONY: ps
ps:
		docker-compose ps